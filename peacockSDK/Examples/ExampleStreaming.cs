﻿/**
* (C) Copyright IBM Corp. 2015, 2020.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*/
/*
 
@author: Subhash Chandra 

 */
#pragma warning disable 0649

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using IBM.Watson.SpeechToText.V1;
using IBM.Cloud.SDK;
using IBM.Cloud.SDK.Authentication;
using IBM.Cloud.SDK.Authentication.Iam;
using IBM.Cloud.SDK.Utilities;
using IBM.Cloud.SDK.DataTypes;

//for text to speech:Start
using IBM.Watson.TextToSpeech.V1;
using IBM.Watson.TextToSpeech.V1.Model;
using IBM.Cloud.SDK.Utilities;
using IBM.Cloud.SDK.Authentication;
using IBM.Cloud.SDK.Authentication.Iam;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IBM.Cloud.SDK;

//for text to speech:end


namespace IBM.Watsson.Examples
{
    public class ExampleStreaming : MonoBehaviour
    {
        #region PLEASE SET THESE VARIABLES IN THE INSPECTOR
        [Space(10)]
        [Tooltip("The service URL (optional). This defaults to \"https://stream.watsonplatform.net/speech-to-text/api\"")]
        [SerializeField]
        private string _serviceUrl;
        [Tooltip("Text field to display the results of streaming.")]
        public Text ResultsField;
        [Header("IAM Authentication")]
        [Tooltip("The IAM apikey.")]
        [SerializeField]
        private string _iamApikey;

        [Header("Parameters")]
        // https://www.ibm.com/watson/developercloud/speech-to-text/api/v1/curl.html?curl#get-model
        [Tooltip("The Model to use. This defaults to en-US_BroadbandModel")]
        [SerializeField]
        private string _recognizeModel;





        #endregion

        // for text to speech credential: Start
        #region PLEASE SET THESE VARIABLES IN THE INSPECTOR
        [Space(10)]
        [Tooltip("The IAM apikey.")]
        [SerializeField]
        private string iamApikey1;
        [Tooltip("The service URL (optional). This defaults to \"https://gateway.watsonplatform.net/text-to-speech/api\"")]
        [SerializeField]
        private string serviceUrl1;
        private TextToSpeechService service;
        private string allisionVoice = "en-US_AllisonV3Voice";
        private string synthesizeText = "Hello, welcome to the Watson Unity SDK!";
        private string placeholderText = "Please type text here and press enter.";
        private string waitingText = "Watson Text to Speech service is synthesizing the audio!";
        private string synthesizeMimeType = "audio/wav";
        public InputField textInput;
        private bool _textEntered = false;
        //private AudioClip _recording = null;
        private byte[] audioStream = null;
        #endregion

        // for text to speech credential: Ends


        private int _recordingRoutine = 0;
        private string _microphoneID = null;
        private AudioClip _recording = null;
        private int _recordingBufferSize = 1;
        private int _recordingHZ = 22050;

        private SpeechToTextService _service;

        void Start()
        {
            LogSystem.InstallDefaultReactors();
            Runnable.Run(CreateService());
            Runnable.Run(CreateService1());   // For text to speech
        }

        private IEnumerator CreateService()
        {
            if (string.IsNullOrEmpty(_iamApikey))
            {
                throw new IBMException("Plesae provide IAM ApiKey for the service.");
            }

            IamAuthenticator authenticator = new IamAuthenticator(apikey: _iamApikey);

            //  Wait for tokendata
            while (!authenticator.CanAuthenticate())
                yield return null;

            _service = new SpeechToTextService(authenticator);
            if (!string.IsNullOrEmpty(_serviceUrl))
            {
                _service.SetServiceUrl(_serviceUrl);
            }
            _service.StreamMultipart = true;

            Active = true;
            StartRecording();
        }
        // test to speech: start
        private IEnumerator CreateService1()
        {
            if (string.IsNullOrEmpty(iamApikey1))
            {
                throw new IBMException("Please add IAM ApiKey to the Iam Apikey field in the inspector.");
            }

            IamAuthenticator authenticator = new IamAuthenticator(apikey: iamApikey1);

            while (!authenticator.CanAuthenticate())
            {
                yield return null;
            }

            service = new TextToSpeechService(authenticator);
            if (!string.IsNullOrEmpty(serviceUrl1))
            {
                service.SetServiceUrl(serviceUrl1);
            }
        }

        // Text to speech: ends

        //text to speech: Start systhesize
        #region Synthesize Example
        private IEnumerator ExampleSynthesize(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                text = synthesizeText;
                Log.Debug("ExampleTextToSpeechV1", "Using default text, please enter your own text in dialog box!");

            }
            byte[] synthesizeResponse = null;
            AudioClip clip = null;
            service.Synthesize(
                callback: (DetailedResponse<byte[]> response, IBMError error) =>
                {
                    synthesizeResponse = response.Result;
                    Log.Debug("ExampleTextToSpeechV1", "Synthesize done!");
                    clip = WaveFile.ParseWAV("myClip", synthesizeResponse);
                    PlayClip(clip);
                },
                text: text,
                voice: allisionVoice,
                accept: synthesizeMimeType
            );

            while (synthesizeResponse == null)
                yield return null;

            yield return new WaitForSeconds(clip.length);
        }
        #endregion

        // text to speech: Ends Systhesize

        // text to speech: Start
        #region PlayClip
        private void PlayClip(AudioClip clip)
        {
            if (Application.isPlaying && clip != null)
            {
                GameObject audioObject = new GameObject("AudioObject");
                AudioSource source = audioObject.AddComponent<AudioSource>();
                source.spatialBlend = 0.0f;
                source.loop = false;
                source.clip = clip;
                source.Play();

                GameObject.Destroy(audioObject, clip.length);
            }
        }
        #endregion

        //Text to speech: Ends

        public bool Active
        {
            get { return _service.IsListening; }
            set
            {
                if (value && !_service.IsListening)
                {
                    _service.RecognizeModel = (string.IsNullOrEmpty(_recognizeModel) ? "en-US_BroadbandModel" : _recognizeModel);
                    _service.DetectSilence = true;
                    _service.EnableWordConfidence = true;
                    _service.EnableTimestamps = true;
                    _service.SilenceThreshold = 0.01f;
                    _service.MaxAlternatives = 1;
                    _service.EnableInterimResults = true;
                    _service.OnError = OnError;
                    _service.InactivityTimeout = -1;
                    _service.ProfanityFilter = false;
                    _service.SmartFormatting = true;
                    _service.SpeakerLabels = false;
                    _service.WordAlternativesThreshold = null;
                    _service.EndOfPhraseSilenceTime = null;
                    _service.StartListening(OnRecognize, OnRecognizeSpeaker);
                }
                else if (!value && _service.IsListening)
                {
                    _service.StopListening();
                }
            }
        }

        private void StartRecording()
        {
            if (_recordingRoutine == 0)
            {
                UnityObjectUtil.StartDestroyQueue();
                _recordingRoutine = Runnable.Run(RecordingHandler());
            }
        }

        private void StopRecording()
        {
            if (_recordingRoutine != 0)
            {
                Microphone.End(_microphoneID);
                Runnable.Stop(_recordingRoutine);
                _recordingRoutine = 0;
            }
        }

        private void OnError(string error)
        {
            Active = false;

            Log.Debug("ExampleStreaming.OnError()", "Error! {0}", error);
        }

        private IEnumerator RecordingHandler()
        {
            Log.Debug("ExampleStreaming.RecordingHandler()", "devices: {0}", Microphone.devices);
            _recording = Microphone.Start(_microphoneID, true, _recordingBufferSize, _recordingHZ);
            yield return null;      // let _recordingRoutine get set..

            if (_recording == null)
            {
                StopRecording();
                yield break;
            }

            bool bFirstBlock = true;
            int midPoint = _recording.samples / 2;
            float[] samples = null;

            while (_recordingRoutine != 0 && _recording != null)
            {
                int writePos = Microphone.GetPosition(_microphoneID);
                if (writePos > _recording.samples || !Microphone.IsRecording(_microphoneID))
                {
                    Log.Error("ExampleStreaming.RecordingHandler()", "Microphone disconnected.");

                    StopRecording();
                    yield break;
                }

                if ((bFirstBlock && writePos >= midPoint)
                  || (!bFirstBlock && writePos < midPoint))
                {
                    // front block is recorded, make a RecordClip and pass it onto our callback.
                    samples = new float[midPoint];
                    _recording.GetData(samples, bFirstBlock ? 0 : midPoint);

                    AudioData record = new AudioData();
                    record.MaxLevel = Mathf.Max(Mathf.Abs(Mathf.Min(samples)), Mathf.Max(samples));
                    record.Clip = AudioClip.Create("Recording", midPoint, _recording.channels, _recordingHZ, false);
                    record.Clip.SetData(samples, 0);

                    _service.OnListen(record);

                    bFirstBlock = !bFirstBlock;
                }
                else
                {
                    // calculate the number of samples remaining until we ready for a block of audio, 
                    // and wait that amount of time it will take to record.
                    int remaining = bFirstBlock ? (midPoint - writePos) : (_recording.samples - writePos);
                    float timeRemaining = (float)remaining / (float)_recordingHZ;

                    yield return new WaitForSeconds(timeRemaining);
                }
            }
            yield break;
        }

        private void OnRecognize(SpeechRecognitionEvent result)
        {
            if (result != null && result.results.Length > 0)
            {
                foreach (var res in result.results)
                {
                    foreach (var alt in res.alternatives)
                    {
                        //string text = string.Format("{0} ({1}, {2:0.00})\n", alt.transcript, res.final ? "Final" : "Interim", alt.confidence); //  this is the original code
                        string text = string.Format("{0} ({1})\n", alt.transcript, res.final ? "Final" : "Interim");    // This is the modified code
                        Log.Debug("ExampleStreaming.OnRecognize()", text);
                        ResultsField.text = text;
                       // Runnable.Run(ExampleSynthesize(ResultsField.text));
                        //Start
                        if (alt.transcript.Contains("what is your name") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "My name is pebo";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("your name") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "My name is pebo";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what name") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "My name is pebo";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("tell me about you") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "I am a pebo, and i can provide assistant during the game. I am created by Indian Institute of Science Bangalore";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("about you") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "I am a pebo, and i can provide assistant during the game. I am created by Indian Institute of Science Bangalore";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("who are you") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "I am a pebo, and i can provide assistant during the game. I am created by Indian Institute of Science Bangalore";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("hi") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "hello how are you";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("hello") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Hi how are you";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("how are you") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "I am awesome";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("okay") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Would you like to ask a question. I would like to help you.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("ok") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Would you like to ask a question. I would like to help you.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Friction is the resistance to motion of one object moving relative to another. When surfaces in contact move relative to each other, the friction between the two surfaces converts kinetic energy into thermal energy. Friction is a non fundamental and non conservative force.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }

                        if (alt.transcript.Contains("friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Friction is the resistance to motion of one object moving relative to another. When surfaces in contact move relative to each other, the friction between the two surfaces converts kinetic energy into thermal energy. Friction is a non fundamental and non conservative force.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Friction is the resistance to motion of one object moving relative to another. When surfaces in contact move relative to each other, the friction between the two surfaces converts kinetic energy into thermal energy. Friction is a non fundamental and non conservative force.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("fiction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Friction is the resistance to motion of one object moving relative to another. When surfaces in contact move relative to each other, the friction between the two surfaces converts kinetic energy into thermal energy. Friction is a non fundamental and non conservative force.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("type of friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two main types of friction, static friction and kinetic friction. Static friction is friction between two or more solid objects that are not moving relative to each other.Kinetic friction, also known as dynamic friction or sliding friction, occurs when two objects are moving relative to each other and rub together.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("type of fiction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two main types of friction, static friction and kinetic friction. Static friction is friction between two or more solid objects that are not moving relative to each other.Kinetic friction, also known as dynamic friction or sliding friction, occurs when two objects are moving relative to each other and rub together.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("type force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two main types of friction, static friction and kinetic friction. Static friction is friction between two or more solid objects that are not moving relative to each other.Kinetic friction, also known as dynamic friction or sliding friction, occurs when two objects are moving relative to each other and rub together.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is static friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Static friction is the friction that exists between a stationary object and the surface on which it's resting.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is static force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Static friction is the friction that exists between a stationary object and the surface on which it's resting.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("static") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Static friction is the friction that exists between a stationary object and the surface on which it's resting.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is kinetic friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Kinetic friction is a force that acts between moving surfaces.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("kinetic") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Kinetic friction is a force that acts between moving surfaces.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is kinetic force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Kinetic friction is a force that acts between moving surfaces.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }

                        // New set of questions
                        if (alt.transcript.Contains("what is coefficient of restitution or resilience") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The ratio of relative velocity of separation after collision to the velocity of approach before collision is called coefficient of restitution resilience(e).It is represented by e and it depends upon the material of the colliding bodies. For a perfectly elastic collision, e = 1, For a perfectly inelastic collision, e = 0, For all other collisions, e reamins between 0 and 1";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is coefficient of resilience") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The ratio of relative velocity of separation after collision to the velocity of approach before collision is called coefficient of restitution resilience(e).It is represented by e and it depends upon the material of the colliding bodies. For a perfectly elastic collision, e = 1, For a perfectly inelastic collision, e = 0, For all other collisions, e reamins between 0 and 1";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("coefficient of resilience") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The ratio of relative velocity of separation after collision to the velocity of approach before collision is called coefficient of restitution resilience(e).It is represented by e and it depends upon the material of the colliding bodies. For a perfectly elastic collision, e = 1, For a perfectly inelastic collision, e = 0, For all other collisions, e reamins between 0 and 1";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("resilience") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The ratio of relative velocity of separation after collision to the velocity of approach before collision is called coefficient of restitution resilience(e).It is represented by e and it depends upon the material of the colliding bodies. For a perfectly elastic collision, e = 1, For a perfectly inelastic collision, e = 0, For all other collisions, e reamins between 0 and 1";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("coefficient of restitution") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The ratio of relative velocity of separation after collision to the velocity of approach before collision is called coefficient of restitution resilience(e).It is represented by e and it depends upon the material of the colliding bodies. For a perfectly elastic collision, e = 1, For a perfectly inelastic collision, e = 0, For all other collisions, e reamins between 0 and 1";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("restitution") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The ratio of relative velocity of separation after collision to the velocity of approach before collision is called coefficient of restitution resilience(e).It is represented by e and it depends upon the material of the colliding bodies. For a perfectly elastic collision, e = 1, For a perfectly inelastic collision, e = 0, For all other collisions, e reamins between 0 and 1";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is inelastic collision") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The collision in which only the momentum remains conserved but kinetic energy does not remain conserved are called inelastic collisions. In an inelastic collision some or all the involved forces are non-conservative forces. Total energy of the system remains conserved. If after the collision two bodies stick to each other, then the collision is said to be perfectly inelastic.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("inelastic collision") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The collision in which only the momentum remains conserved but kinetic energy does not remain conserved are called inelastic collisions. In an inelastic collision some or all the involved forces are non-conservative forces. Total energy of the system remains conserved. If after the collision two bodies stick to each other, then the collision is said to be perfectly inelastic.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is elastic collision") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The collision in which both the momentum and the kinetic energy of the system remains conserved are called elastic collisions. In an elastic collision all the involved forces are conservative forces. Total energy remains conserved.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("elastic collision") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The collision in which both the momentum and the kinetic energy of the system remains conserved are called elastic collisions. In an elastic collision all the involved forces are conservative forces. Total energy remains conserved.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains(" what is collision") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Collision between two or more particles is the interaction for a short interval of time in which they apply relatively strong forces on each other.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("collision") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Collision between two or more particles is the interaction for a short interval of time in which they apply relatively strong forces on each other.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("types of collision") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two types of collision. 1. elastic collision. 2. inelastic collision";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("type of collision") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two types of collision. 1. elastic collision. 2. inelastic collision";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what do you mean by principle of conservation of mechanical energy") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "For conservative forces the sum of kinetic and potential energy of any object remains constant throughout the motion. According to quantum physics, mass and energy are not conserved separately but are conserved as a single entity called mass-energy.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is principle of conservation of mechanical energy") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "For conservative forces the sum of kinetic and potential energy of any object remains constant throughout the motion. According to quantum physics, mass and energy are not conserved separately but are conserved as a single entity called mass-energy.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("principle of conservation of mechanical energy") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "For conservative forces the sum of kinetic and potential energy of any object remains constant throughout the motion. According to quantum physics, mass and energy are not conserved separately but are conserved as a single entity called mass-energy.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what do you mean by principle of conservation of energy") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The sum of all kinds of energies in an isolated system remains constant at all times.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is principle of conservation of energy") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The sum of all kinds of energies in an isolated system remains constant at all times.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("principle of conservation of energy") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The sum of all kinds of energies in an isolated system remains constant at all times.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what are Newton’s laws of motion") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "First law states that.-If the net external force on a body is zero, its acceleration is zero.  Acceleration can be non zero only if there is a net external force on the body. Second law states that. -The rate of change of momentum of a body is directly proportional to the applied force and takes place in the direction in which the force acts. Third law states that. -To every action, there is always an equal and opposite reaction.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is Newton’s laws of motion") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "First law states that.-If the net external force on a body is zero, its acceleration is zero.  Acceleration can be non zero only if there is a net external force on the body. Second law states that. -The rate of change of momentum of a body is directly proportional to the applied force and takes place in the direction in which the force acts. Third law states that. -To every action, there is always an equal and opposite reaction.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("Newton’s laws of motion") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "First law states that.-If the net external force on a body is zero, its acceleration is zero.  Acceleration can be non zero only if there is a net external force on the body. Second law states that. -The rate of change of momentum of a body is directly proportional to the applied force and takes place in the direction in which the force acts. Third law states that. -To every action, there is always an equal and opposite reaction.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is horizontal range ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "It is defined as the maximum distance covered in horizontal distance. R = u squire sine two theta divided by g";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("horizontal range ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "It is defined as the maximum distance covered in horizontal distance. R = u squire sine two theta divided by g";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is maximum height ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "It is defined as the maximum vertical distance covered by projectile. H = u squire sine squire theta divide by two g";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("maximum height ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "It is defined as the maximum vertical distance covered by projectile. H = u squire sine squire theta divide by two g";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is time of flight ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "It is defined as the total time for which the projectile remains in air. T = two u sine theta divided by g. where u is the velocity with which the ball is thrown.theta is the projectile angle.g is the gravity that is 9.8 metre per second square.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("time of flight ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "It is defined as the total time for which the projectile remains in air. T = two u sine theta divided by g. where u is the velocity with which the ball is thrown.theta is the projectile angle.g is the gravity that is 9.8 metre per second square.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is projectile motion") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "When any object is thrown from horizontal at an angle theta except 90°, then the path followed by it is called trajectory, the object is called projectile and its motion is called projectile motion.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("projectile motion") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "When any object is thrown from horizontal at an angle theta except 90°, then the path followed by it is called trajectory, the object is called projectile and its motion is called projectile motion.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is moment of force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "If a force is applied to the end of an object whose other end is attached to a pivot or hinge, the force will tend to rotate the object about the pivot, called the fulcrum, Thus, a force can, in certain circumstances, have a turning effect. We call this effect the Moment of the Force. It is equal to the product of the magnitude of the force and the perpendicular distance of the line of action of force from the axis of rotation. It is a vector quantity.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is moment") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "If a force is applied to the end of an object whose other end is attached to a pivot or hinge, the force will tend to rotate the object about the pivot, called the fulcrum, Thus, a force can, in certain circumstances, have a turning effect. We call this effect the Moment of the Force. It is equal to the product of the magnitude of the force and the perpendicular distance of the line of action of force from the axis of rotation. It is a vector quantity.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("moment") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "If a force is applied to the end of an object whose other end is attached to a pivot or hinge, the force will tend to rotate the object about the pivot, called the fulcrum, Thus, a force can, in certain circumstances, have a turning effect. We call this effect the Moment of the Force. It is equal to the product of the magnitude of the force and the perpendicular distance of the line of action of force from the axis of rotation. It is a vector quantity.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is meant by turning effect of force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The turning effect of a force acting on a body about an axis is due to the moment of force or torque. It depends on the magnitude of a force applied and the distance of the line of action of force from the axis of rotation.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is meant by turning effect") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The turning effect of a force acting on a body about an axis is due to the moment of force or torque. It depends on the magnitude of a force applied and the distance of the line of action of force from the axis of rotation.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is turning effect") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The turning effect of a force acting on a body about an axis is due to the moment of force or torque. It depends on the magnitude of a force applied and the distance of the line of action of force from the axis of rotation.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("turning effect") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The turning effect of a force acting on a body about an axis is due to the moment of force or torque. It depends on the magnitude of a force applied and the distance of the line of action of force from the axis of rotation.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what are gravitational forces") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "It is the force on a body due to the earth's attraction is called the force of Gravity. In the universe, all particles attract one another due to its mass";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is gravitational force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "It is the force on a body due to the earth's attraction is called the force of Gravity. In the universe, all particles attract one another due to its mass";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("gravitational force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "It is the force on a body due to the earth's attraction is called the force of Gravity. In the universe, all particles attract one another due to its mass";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what are non contact forces") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The forces experienced by bodies even without being physically touched are called the Non-Contact Forces or the Forces at a distance.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what are non contact force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The forces experienced by bodies even without being physically touched are called the Non-Contact Forces or the Forces at a distance.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("non contact forces") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The forces experienced by bodies even without being physically touched are called the Non-Contact Forces or the Forces at a distance.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("non contact force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The forces experienced by bodies even without being physically touched are called the Non-Contact Forces or the Forces at a distance.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what are contact forces") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The forces which act on bodies when they are in physical contact are called the Contact forces.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is contact forces") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The forces which act on bodies when they are in physical contact are called the Contact forces.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is contact force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The forces which act on bodies when they are in physical contact are called the Contact forces.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("contact forces") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The forces which act on bodies when they are in physical contact are called the Contact forces.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("contact force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The forces which act on bodies when they are in physical contact are called the Contact forces.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "A Force is that physical cause which changes or tends to change either the size or shape or the state of rest or motion of the body.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is angle of friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "In the case of limiting friction, the angle which the resultant R of the limiting force f s and the normal reaction N makes with the normal reaction N is called the angle of friction.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("angle of friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "In the case of limiting friction, the angle which the resultant R of the limiting force f s and the normal reaction N makes with the normal reaction N is called the angle of friction.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("angle of fiction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "In the case of limiting friction, the angle which the resultant R of the limiting force f s and the normal reaction N makes with the normal reaction N is called the angle of friction.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is law of limiting friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The limiting maximum static frictional force depends upon the nature of the surfaces in contact. It does not depend upon the size or area of the surfaces. For the given surfaces, the limiting frictional force f s is directly proportional to the normal reaction N. F s is equal to mu s multiplied by N. Where the constant of proportionality mu s is called the coefficient of static friction. The said formula holds only when f s has its maximum, limiting value.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("law of limiting friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The limiting maximum static frictional force depends upon the nature of the surfaces in contact. It does not depend upon the size or area of the surfaces. For the given surfaces, the limiting frictional force f s is directly proportional to the normal reaction N. F s is equal to mu s multiplied by N. Where the constant of proportionality mu s is called the coefficient of static friction. The said formula holds only when f s has its maximum, limiting value.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("name different types of friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two main types of friction, static friction and kinetic friction.  Static friction is friction between two or more solid objects that are not moving relative to each other.Kinetic friction, also known as dynamic friction or sliding friction, occurs when two objects are moving relative to each other and rub together.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is the coefficient of friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "A coefficient of friction is a dimensionless scalar value that shows the relationship between two objects and the normal reaction between the objects that are involved.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is coefficient of friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "A coefficient of friction is a dimensionless scalar value that shows the relationship between two objects and the normal reaction between the objects that are involved.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("coefficient of friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "A coefficient of friction is a dimensionless scalar value that shows the relationship between two objects and the normal reaction between the objects that are involved.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is the friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Friction is the resistance to motion of one object moving relative to another. When surfaces in contact move relative to each other, the friction between the two surfaces converts kinetic energy into thermal energy. Friction is a non fundamental and non conservative force.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is the friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Friction is the resistance to motion of one object moving relative to another. When surfaces in contact move relative to each other, the friction between the two surfaces converts kinetic energy into thermal energy. Friction is a non fundamental and non conservative force.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }
                        if (alt.transcript.Contains("what is fiction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Friction is the resistance to motion of one object moving relative to another. When surfaces in contact move relative to each other, the friction between the two surfaces converts kinetic energy into thermal energy. Friction is a non fundamental and non conservative force.";
                            ResultsField.text = text;
                            Runnable.Run(ExampleSynthesize(ResultsField.text));
                        }

                        //End

                    }

                    if (res.keywords_result != null && res.keywords_result.keyword != null)
                    {
                        foreach (var keyword in res.keywords_result.keyword)
                        {
                            Log.Debug("ExampleStreaming.OnRecognize()", "keyword: {0}, confidence: {1}, start time: {2}, end time: {3}", keyword.normalized_text, keyword.confidence, keyword.start_time, keyword.end_time);
                        }
                    }

                    if (res.word_alternatives != null)
                    {
                        foreach (var wordAlternative in res.word_alternatives)
                        {
                            Log.Debug("ExampleStreaming.OnRecognize()", "Word alternatives found. Start time: {0} | EndTime: {1}", wordAlternative.start_time, wordAlternative.end_time);
                            foreach (var alternative in wordAlternative.alternatives)
                                Log.Debug("ExampleStreaming.OnRecognize()", "\t word: {0} | confidence: {1}", alternative.word, alternative.confidence);
                        }
                    }
                }
            }
        }

        private void OnRecognizeSpeaker(SpeakerRecognitionEvent result)
        {
            if (result != null)
            {
                foreach (SpeakerLabelsResult labelResult in result.speaker_labels)
                {
                    Log.Debug("ExampleStreaming.OnRecognizeSpeaker()", string.Format("speaker result: {0} | confidence: {3} | from: {1} | to: {2}", labelResult.speaker, labelResult.from, labelResult.to, labelResult.confidence));
                }
            }
        }
    }
}
